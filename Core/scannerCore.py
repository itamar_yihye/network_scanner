import scapy.all as scapy
import time
import threading
import requests
import sys
import json

this_device_ip = ""
devices = []
stop_threads = False


def get_mac_vendor(mac_address):
    """
        a function that uses the macvendors api to guess
        the mac adress
    """
    url = f"https://api.macvendors.com/{mac_address}"
    response = requests.get(url)
    if response.status_code == 200:
        return response.text.strip()
    return "Unknown"

def change_last_ip_number(ip_address, number):
    """
        a function that gets an ip adress and a number
        and changes the last value of the ip to that number
    """
    return '.'.join(ip_address.split('.')[:-1] + [str(number)])

def get_device_ip():
    return scapy.get_if_addr(scapy.conf.iface)

def send_arp_packet(ip):
    arp_request = scapy.ARP(pdst=ip)
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast / arp_request
    scapy.sendp(arp_request_broadcast, verbose=False)

def sniff_arp_packets():
    
    def add_to_devices(packet):
        arp_layer = packet[scapy.ARP]
        ip = arp_layer.psrc
        if ip != this_device_ip:
            mac = arp_layer.hwsrc
            adapter_name = get_mac_vendor(arp_layer.hwsrc)
            
            data = {
                "ip_address": ip,
                "mac_address": mac,
                "adapter_name": adapter_name
            }
            json_data = json.dumps(data)
            
            # Write the JSON data as a separate line
            sys.stdout.write(f"{json_data}\n")
            sys.stdout.flush()

    def stopfilter(packet):
        return stop_threads
        
    scapy.sniff(count=0, filter="arp[6:2] = 2", prn=add_to_devices, stop_filter=stopfilter)

def scan_subnet(device_ip):
    global stop_threads
    
    try:
        for i in range(255):
            send_arp_packet(change_last_ip_number(ip_address=device_ip, number=i))
            time.sleep(0.2)
    finally:
        stop_threads = True

this_device_ip = get_device_ip()
# Create threads
sniff_thread = threading.Thread(target=sniff_arp_packets)
scan_thread = threading.Thread(target=scan_subnet, args=(this_device_ip,))

# Start threads
sniff_thread.start()
scan_thread.start()

# Wait for both threads to finish
sniff_thread.join()
scan_thread.join()
