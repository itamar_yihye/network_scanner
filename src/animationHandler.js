var scanBtn;

window.addEventListener("load", ()=>{
    // getting the scan starting point
    scanBtn = document.getElementsByClassName("scan-button")[0];
    scanBtn.addEventListener("click", ScanHandler);
})

// a function that starts the scan
function ScanHandler(e){
    if(e.target.classList.contains("welcome-page-button"))
    {
        playStartScanAnimation();
    }
}

// a function that plays the radar animation
function playStartScanAnimation()
{
    // play reverse glass animation
    console.log("animation playing");
    playGlassLeaveAnimation();
    playTextAndButtonsLeaveAnimation();
    playScanningAnimation();
    setTimeout(()=>{
        playEndScanningAnimation();
        setTimeout(()=>{openAppInterface()}, 1500);
    }, 3000);
}

function playGlassLeaveAnimation()
{
    let glass1 = document.getElementsByClassName("glass1")[0];
    let glass2 = document.getElementsByClassName("glass2")[0];
    let glass3 = document.getElementsByClassName("glass3")[0];
    let glass4 = document.getElementsByClassName("glass4")[0];
    playObjAnimation(glass1, "glass-one-leave", "2s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(glass2, "glass-two-leave", "2s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(glass3, "glass-three-leave", "2s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(glass4, "glass-four-leave", "2s", "ease-in-out", "0s", "1", "normal", "forwards");
    setTimeout(()=>{
        let glasses = document.getElementsByClassName("glass");
        Array.from(glasses).forEach(function(element) {
          element.remove();
        });
    }, 2000);
}

function playTextAndButtonsLeaveAnimation()
{
    let mainText = document.getElementsByClassName("welcome-text")[0];
    let startBtn = document.getElementsByClassName("welcome-page-button")[0];
    playObjAnimation(mainText, "disappear-to-nothing", "1s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(startBtn, "disappear-to-nothing", "1s", "ease-in-out", "0s", "1", "normal", "forwards");
    setTimeout(()=>{
        document.getElementsByClassName("welcome-text")[0].remove();
        document.getElementsByClassName("welcome-page-button")[0].remove();
    }, 1000);
}

// a function that handles the radar animation
function playScanningAnimation()
{
    let welcomeContainer = document.getElementsByClassName("welcome-container")[0];
    welcomeContainer.innerHTML += `
    <div class="radar-container">
        <div class="radar">
            <div class="vertical-line"></div>
            <div class="horizontal-line"></div>
            <div class="ring ring-1"></div>
            <div class="ring ring-2"></div>
            <div class="ring ring-3"></div>
            <div class="ring ring-4"></div>
            <div class="ring ring-5"></div>
            <div class="ring ring-6"></div>
        </div>
    </div>
    `.trim();
    setTimeout(()=>{
        var welcomeContainer = document.querySelector('.radar-container');
        var spinningLine = document.createElement('div');
        spinningLine.className = 'spinning-line';
        var secondChild = welcomeContainer.children[1];
        welcomeContainer.insertBefore(spinningLine, secondChild);
    }, 1000);

}

// a function to play the radar close animation
function playEndScanningAnimation()
{
    // stop the spinning line animation
    document.getElementsByClassName("spinning-line")[0].remove();
    // handle radar leave animation
    let rings = document.getElementsByClassName("ring");
    let verticalLine = document.getElementsByClassName("vertical-line")[0];
    let horizontalLine = document.getElementsByClassName("horizontal-line")[0];
    playObjAnimation(rings[0], "ring-leave", "0.6s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(rings[1], "ring-leave", "1s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(rings[2], "ring-leave", "0.8s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(rings[3], "ring-leave", "0.4s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(rings[4], "ring-leave", "0.7s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(rings[5], "ring-leave", "0.5s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(verticalLine, "vertical-line-leave", "0.4s", "ease-in-out", "0s", "1", "normal", "forwards");
    playObjAnimation(horizontalLine, "horizontal-line-leave", "0.6", "ease-in-out", "0s", "1", "normal", "forwards");
    
    // delete all the unnecessary elements
    setTimeout(()=>{
        document.getElementsByClassName("radar-container")[0].remove();
    }, 1000);
}

// a function that openes the main interface
function openAppInterface()
{
    window.location.href = "./index.html";
}

// a simple function that is a constructor of the animation feature
function playObjAnimation(obj, name, duration, timingFunction, delay, iterationCount, direction, fillMode) {
    obj.style.animationName = name;
    obj.style.animationDuration = duration;
    obj.style.animationTimingFunction = timingFunction;
    obj.style.animationDelay = delay;
    obj.style.animationIterationCount = iterationCount;
    obj.style.animationDirection = direction;
    obj.style.animationFillMode = fillMode;
}