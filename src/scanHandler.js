let {PythonShell} = require('python-shell');  

let routerImage = "./Images/router.svg";
let deviceIconList = [
    "./Images/red_device.svg",
     "./Images/green_device.svg",
     "./Images/blue_device.svg",
     "./Images/teal_device.svg",
     "./Images/purple_device.svg"
     ];

window.addEventListener("load", ()=>{
    let deviceList = document.getElementsByClassName("device-list")[0];
    deviceList.addEventListener("click", handleDeviceClicked)
    scanForDevices();
})

function scanForDevices()
{
    let shell = new PythonShell('./Core/scannerCore.py', {mode: "json"});

    console.log(shell);
    shell.on('message', function (message) {
        console.log(message);
        addDeviceToDeviceList(deviceIconList[Math.floor(Math.random() * deviceIconList.length)], message.ip_address, message.mac_address, message.adapter_name);
    });
    shell.end(function (err,code,signal) {
        if (err) throw err;
        console.log('The exit code was: ' + code);
        console.log('The exit signal was: ' + signal);
        console.log('finished');
    });
}

function addDeviceToDeviceList(icon, ip, mac, adapterName)
{
    let elem = document.createElement("li");
    elem.setAttribute("data-device-ip", ip);
    elem.setAttribute("data-device-mac", mac);
    elem.setAttribute("data-device-adapter-name", adapterName);
    let image = document.createElement("img");
    image.src = icon;
    image.style.height = "50px";
    image.style.width = "50px";
    let h1Element = document.createElement("h1");
    h1Element.textContent = ip;
    elem.appendChild(image);
    elem.appendChild(h1Element);
    let ulElement = document.querySelector(".device-list");
    ulElement.appendChild(elem);
}

function handleDeviceClicked(e)
{
    if(e.target.tagName === 'LI')
    {
        removeActiveClassFromAll();
        e.target.classList.add("active");
        loadDeviceToInspector(e.target);
    }
}

function loadDeviceToInspector(device)
{
    let dataHolder = document.getElementsByClassName("inspector-data-holder")[0];
    dataHolder.innerHTML = `
        <img src="`+ device.querySelector('img').src +`" />
        <h2>IP Adress: ` + device.getAttribute("data-device-ip") +`</h2>
        <h2>MAC Adress: ` + device.getAttribute("data-device-mac") +`</h2>
        <h2>Adapter Name: ` + device.getAttribute("data-device-adapter-name") +`</h2>
    `;
}

function removeActiveClassFromAll()
{
    let deviceList = document.getElementsByClassName("device-list")[0];
    let liElements = deviceList.getElementsByTagName('li');

    // Remove the "active" class from each <li> element
    for (var i = 0; i < liElements.length; i++) {
        liElements[i].classList.remove('active');
    }
}